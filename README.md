# ioc-edx-theme

This repository contains the OpenEdX theme used by IONIS Online Courses.

It is based on Stanford’s [works](https://github.com/Stanford-Online/edx-theme).

If you have any comments or questions, please contact digital.learning@ionis-group.com.
